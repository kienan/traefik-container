# Traefik Container

## Running in development

```
podman network exists web || podman network create web
podman-compose -f container-compose.yml -f container-compose.dev.yml up -d
```

## Running in production

```
# Create a place to store configuration snippets for the file provider, if one
# does not yet exist. A known external volume is used since podman-compose will prefix
# non-external volumes with the project slug, making it less predictable for
# production deployments.
sudo podman volume exists traefik_config || sudo podman volume create traefik_config
sudo podman-compose up -d
```

It is possible to run this with a systemd service.
